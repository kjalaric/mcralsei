"""
This is a one-off bot for a minecraft server, please don't expect any serious documentation...
"""
import asyncio
import time
import random
import string
from discord.ext import commands, tasks  # pip install discord.py
from mcstatus import MinecraftServer  # pip install mcstatus
import serverhandler

# defs
confirmation_timeout_in_seconds = 30

# globals
mcserver = serverhandler.ServerHandler()
service_active = False  # an internal flag that says whether the server is running. mostly used for the watchdog
watchdog_patted = True   # an internal flag; when toggled to True from false, the server sends a message to the dev channel
active_player_check_failed = False  # an internal flag used to indicate whether a previous status message had no one playing. used for server spindown control
permanently_on = False  # a command flag that latches the server on (i.e. it doesn't switch off if no one is playing)
stop_request_time = None  # holds the time of the last !stop request, so another !stop within the next x seconds can shut the server down


helptext = "I'm Ralsei! I have the following ACTs:" \
           "\n\t**!help, !h** - get this message!" \
           "\n\t**!status** - find out whether or not the minecraft server is running" \
           "\n\t**!start** - start the minecraft server, if it isn't running but I'm online" \
           "\n\t**!stop** - stop the server manually, if no one is playing" \
           f"\n\t**!vote** - vote for one of [{list(votes.keys())}] and if everyone agrees within 30 seconds, I will use my magic to make it so!"

# get bot properties
bot_properties = dict()
with open("bot.properties", "r") as fi:
    for line in fi.readlines():
        try:
            splitline = [x.strip() for x in line.split("=")]
            bot_properties[splitline[0]] = splitline[1]
        except:
            print(f"Bad line in bot.properties: {line}")

# get admins
admins = set()
with open("admin.properties", "r") as fi:
    for line in fi.readlines():
        try:
            admins.add(line.strip())
        except:
            print(f"Bad line in admin.properties: {line}")



client = commands.Bot(
    command_prefix='!',
    help_command=None
)


def getAuthor(ctx):
    return str(ctx.author).strip()


def confirmAdmin(ctx):
    """
    Make sure that I'm sending the debug commands. Raise an exception otherwise.
    :param ctx: message context
    :return:
    """
    if getAuthor(ctx) not in admins:
        raise Exception(f"{ctx.author} tried to use a mod command.")


@client.command()
async def help(ctx):
    """
    Bot command to display helptext.
    :param ctx: message context (internal)
    :return: None
    """
    await ctx.send(helptext)


@client.command()
async def h(ctx):
    """
    Bot command to display helptext.
    :param ctx: message context (internal)
    :return: None
    """
    await ctx.send(helptext)


@client.command()
async def start(ctx):
    """
    Bot command to start the server, if it isn't already started.
    :param ctx: message context (internal)
    :return: None
    """
    try:
        server = MinecraftServer.lookup("127.0.0.1:25565")
        server_status = server.status()

        if server_status.players.online:
            already_on = True
        else:
            already_on = False
    except:  # not sure if this raises exception or just returns False by default
        already_on = False

    if already_on:
        await ctx.send(f"The server is already online! There are {server_status.players.online} players connected.")
    else:
        await ctx.send('Starting server! Give me a sec...')
        try:
            mcserver.start()
        except Exception as e:
            print(e)


@client.command()
async def stop(ctx):
    """
    Bot command to stop the server if no one is currently playing.
    :param ctx: message context (internal)
    :return: None
    """
    global stop_request_time
    global service_active
    current_players = 0
    current_time = time.time()

    try:
        server = MinecraftServer.lookup("127.0.0.1:25565")
        server_status = server.status()
        current_players = server_status.players.online
    except:
        pass

    if current_players > 0:
        await ctx.send(f"There are currently players ({current_players}) on the server, so I'm not going to listen to you!")
    else:
        if stop_request_time is None or (current_time - stop_request_time) > 10:
            stop_request_time = current_time
            await ctx.send('To manually stop the server, please use !stop again within 10 seconds.')
        else:
            await ctx.send('Stopping server. Thanks!')
            service_active = False  # sure hope we don't end up in a race condition
            mcserver.stop()


@client.command()
async def _stop(ctx):
    """
    Bot command to stop the server even if someone is playing.
    :param ctx: message context (internal)
    :return: None
    """
    global stop_request_time
    global service_active
    current_players = 0
    current_time = time.time()

    try:
        server = MinecraftServer.lookup("127.0.0.1:25565")
        server_status = server.status()
    except:
        pass

    if stop_request_time is None or (current_time - stop_request_time) > 10:
        stop_request_time = current_time
        await ctx.send('To manually stop the server, please use !_stop again within 10 seconds.')
    else:
        await ctx.send('Stopping server. Thanks!')
        service_active = False  # sure hope we don't end up in a race condition
        mcserver.stop()


@client.command()
async def latch(ctx):
    """
    Bot command to keep the server running when no players are active (or disable this behaviour)
    :param ctx: message context (internal)
    :return: None
    """
    global permanently_on
    confirmAdmin(ctx)
    if permanently_on:
        permanently_on = False
        await ctx.send('Server is delatched and will switch off after 10 minutes of 0 players.')
    else:
        permanently_on = True
        await ctx.send('Server is latched and will stay on permanently.')


@client.command()
async def status(ctx):
    """
    Bot command to provide an indicator of server activity.
    :param ctx: message context (internal)
    :return: None
    """
    global service_active
    global permanently_on
    server = MinecraftServer.lookup("127.0.0.1:25565")
    borked = False

    if service_active:
        try:
            server_status = server.status()
            status_message = f"The server has been online since {mcserver.uptimeAsString()}.\n" \
                             f"There are {server_status.players.online} players connected.\n" \
                             f"Server latch state is {permanently_on}."
        except:
            borked = True
    else:
        status_message = f"The server is offline."

    if borked:
        await ctx.send('The server is currently [[DATA EXPUNGED]]')
    else:
        await ctx.send(status_message)


@client.command()
async def say(ctx, *args):
    confirmAdmin(ctx)
    mcserver.sendRcon(f"/say Ralsei says: {' '.join(args)}")


@client.command()
async def whitelist(ctx, arg):
    confirmAdmin(ctx)
    mcserver.sendRcon(f"/whitelist add {arg}")


@client.command()
async def save(ctx):
    confirmAdmin(ctx)
    mcserver.sendRcon(f"/save-all")


# watchdog/service checker
async def serviceCheck():
    global service_active
    global watchdog_patted
    borked = False
    server = None

    # if the service is running, act as a watchdog and alert the dev channel if something goes wrong
    # if the service is not running, keep checking to see when it is, then update the server-status channel

    while not client.is_closed():
        if not service_active:
            try:
                server = MinecraftServer.lookup("127.0.0.1:25565")
                server.status()
                service_active = True
                await client.get_channel(bot_properties['server_status_channel_id']).send("The server is now available!")
            except:
                pass
        else:
            try:
                server.status()
            except:
                borked = True

        if borked:
            if watchdog_patted:
                watchdog_patted = False
                await client.get_channel(bot_properties['automessage_channel_id']).send("Watchdog wasn't patted! Something went wrong.")

        await asyncio.sleep(60)  # check every 1 minute


# active player check - turn off the server if people aren't on
async def activePlayerCheck():
    global permanently_on
    global active_player_check_failed
    global service_active
    await client.wait_until_ready()
    while not client.is_closed():
        if not permanently_on:
            try:
                server = MinecraftServer.lookup("127.0.0.1:25565")
                channel = client.get_channel(bot_properties['server_status_channel_id'])
                players_online = server.status().players.online
                if players_online == 0:
                    if active_player_check_failed:
                        await channel.send("Server is shutting down due to zero current players. To restart, use !start")
                        service_active = False
                        mcserver.stop()
                    else:
                        active_player_check_failed = True
                else:
                    active_player_check_failed = False
            except Exception as e:
                print(e)
        await asyncio.sleep(300)  # check every 5 minutes


@client.event
async def on_ready():
    await client.get_channel(bot_properties['server_status_channel_id']).send("Hello! Use the command !start to start the server, or !help to see what else I can do.")


client.loop.create_task(activePlayerCheck())
client.loop.create_task(serviceCheck())


# start bot
client.run(
    bot_properties['token']
)
