# mcralsei
Discord bot to control a minecraft server.

## dependencies
discord.py, mcrcon, mcstatus (get them all from pip)

## how to do things
I can't guarantee it'll work out of the box. I'd recommend using the following directory structure:
- /opt/minecraft
- - /ralsei: clone this code in here
- - /server: put your MC server jar etc. here
Update admin.properties with the discord tags (name#number) of people who will be able to use admin commands.
Update bot.properties with the bot token and a few channel IDs where it can make announcements.

In a linux screen (yum install screen, or apt install screen), run the script from the MC server directory with sudo python ../ralsei.bot. Feel free to do this differently if you know better, I'm just assuming that we didn't spend time working on any permissions.

## contributing
I am a one-man army.

## acknowledgments
Thank you Toby Fox for indirectly giving this project a name.

## license
MIT License

